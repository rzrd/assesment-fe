import React from 'react';
import { Button, Form, FormGroup, Input, FormText } from 'reactstrap';
import { Link, Redirect } from "react-router-dom";


export default class Login extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         email: '',
         password: '',
      }
      this.onChangeEmail = this.onChangeEmail.bind(this)
      this.onChangePassword = this.onChangePassword.bind(this)
      this.userLogin = this.userLogin.bind(this)
   }
   onChangeEmail(event) {
      this.setState({
         email: event.target.value
      })
   }
   onChangePassword(event) {
      this.setState({
         password: event.target.value
      })
   }
   userLogin() {
      fetch('https://test-binar.herokuapp.com/auth/login',
         {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
               'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
               email: this.state.email,
               password: this.state.password
            }),
         })
         .then(response => response.json())
         .then(data => {
            if (data.result == null) {
               this.props.history.push('/')
            } else {
               localStorage.setItem('isLoggedIn', true)
               localStorage.setItem('token', data.result.access_token)
               this.props.history.push('/dashboard')
            }
         })
         .catch(err => {
            console.log(err)
         })
   }

   render() {
      if (localStorage.getItem('isLoggedIn')) {
         return (
            <Redirect to='/dashboard'></Redirect>
         )
      }
      return (
         <div>
            <Form style={{ width: '40vw', margin: 'auto' }}>
               <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                  <Input type="email" onChange={this.onChangeEmail} placeholder="Email" required />
               </FormGroup>
               <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                  <Input type="password" onChange={this.onChangePassword} placeholder="Password" required />
               </FormGroup>
               <Button onClick={this.userLogin}>Log In</Button>
            </Form>
            <FormText>Belum punya akun? daftar disini..</FormText>
            <Link to='/register'><Button>register</Button></Link>
         </div>
      );
   }
}