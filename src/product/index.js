import React from 'react';
import {
    Card, CardImg, CardTitle, CardColumns,
    CardSubtitle, CardBody, Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Input
} from 'reactstrap';
import Header from '../header';



export default class Dashboard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            productName: '',
            price: '',
            imageUrl: '',
            productId: null,
            isOpen: false,
            modal: false,
            modal2: false,

        }
        this.btntoggle = this.btntoggle.bind(this)
        this.btntoggle2 = this.btntoggle2.bind(this)

        this.toggle = this.toggle.bind(this);
        this.toggle2 = this.toggle2.bind(this);

        this.onChangeProductName = this.onChangeProductName.bind(this)
        this.onChangePrice = this.onChangePrice.bind(this)
        this.onChangeImageUrl = this.onChangeImageUrl.bind(this)
        this.deleteProduct = this.deleteProduct.bind(this)
    }
    onChangeProductName(event) {
        this.setState({
            productName: event.target.value
        })
    }
    onChangePrice(event) {
        this.setState({
            price: event.target.value
        })
    }
    onChangeImageUrl(event) {
        this.setState({
            imageUrl: event.target.value
        })
    }

    componentDidMount() {
        fetch(`https://test-binar.herokuapp.com/v1/products/`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
            })
            .then(response => response.json())
            .then(data => {
                this.setState(state => ({
                    products: [...state.products, ...data.result]
                }))
            })
            .catch(err => {
                console.log(err)
            })
    }
    deleteProduct() {
        fetch(`https://test-binar.herokuapp.com/v1/products/${this.state.productId}`,
            {
                method: 'DELETE',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                redirect: 'follow',
                referrer: 'no-referrer',
            })
            .then(() => {
                this.setState({
                    modal: false,
                    productName: '',
                    productId: '',
                });
                window.location.reload();
            })
            .catch(err => {
                console.log(err);
            })
    }
    editProduct() {
        fetch(`https://test-binar.herokuapp.com/v1/products/112`,
            {
                method: 'PUT',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `${localStorage.getItem('token')}`
                },
                redirect: 'follow',
                referrer: 'no-referrer',
                body: JSON.stringify({
                    name: this.state.productName,
                    price: this.state.price,
                    imageurl: this.state.imageUrl,
                }),
            })
            .then(response => response.json())
            .then(() => {
                this.setState({
                    modal2: false,
                    productName: '',
                    productId: '',
                    imageUrl: ''
                })
            })
            .catch(err => {
                console.log(err);
            })
    }
    btntoggle() {
        this.setState(prevState => ({ modal: !prevState.modal }));
    }
    toggle() {
        this.setState({ isOpen: !this.state.isOpen });
    }
    btntoggle2() {
        this.setState(prevState => ({ modal2: !prevState.modal2 }));
    }
    toggle2() {
        this.setState({ isOpen2: !this.state.isOpen2 });
    }

    render() {
        return (
            <div>
                <Header></Header>
                <h2 >All Product</h2>
                <CardColumns>
                    {this.state.products.map(obj => {
                        return (
                            <div key={obj.id}>
                                <Card body>
                                    <CardImg top width="100%" src={obj.imageurl ? obj.imageurl : `https://ui-avatars.com/api/?name=${obj.name}`} alt={obj.name} />
                                    <CardBody>
                                        <CardTitle>{obj.name}</CardTitle>
                                        <CardSubtitle>{obj.price}</CardSubtitle>
                                    </CardBody>
                                    <Button
                                        onClick={() => this.setState({
                                            modal: true,
                                            productId: obj.id,
                                            productName: obj.name
                                        })}
                                    >
                                        Delete
                                        </Button>
                                    <Button
                                        onClick={() => this.setState({
                                            modal2: true,
                                            productId: obj.id,
                                            productName: obj.name,
                                            price: obj.price,
                                            imageUrl: obj.imageurl
                                        })}
                                    >
                                        Edit
                                        </Button>
                                </Card>
                            </div>
                        )
                    })}
                </CardColumns>

                <Modal isOpen={this.state.modal} toggle={this.btntoggle}>
                    <ModalBody>
                        Are you sure want to delete {this.state.productName}
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.btntoggle}>No</Button>
                        <Button onClick={this.deleteProduct}>Yes, delete it</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal2} toggle={this.btntoggle2}>
                    <ModalHeader>Create New</ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Input type="text" value={this.state.productName} onChange={this.onChangeProductName} placeholder="Product Name" required />
                        </FormGroup>
                        <FormGroup>
                            <Input type="number" value={this.state.price} onChange={this.onChangePrice} placeholder="Price" required />
                        </FormGroup>
                        <FormGroup>
                            <Input type="text" value={this.state.imageUrl} onChange={this.onChangeImageUrl} placeholder="Image URL" required />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.btntoggle2}>Back</Button>
                        <Button onClick={this.editProduct}>Update</Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
};