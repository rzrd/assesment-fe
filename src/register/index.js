import React from 'react';
import { Button, Form, FormGroup, Input, FormText } from 'reactstrap';
import { Link, Redirect } from "react-router-dom";

export default class Register extends React.Component {
   constructor(props) {
      super(props)
      this.state = {
         name: '',
         email: '',
         password: '',
         redirect: false
      }
      this.onChangeName = this.onChangeName.bind(this)
      this.onChangeEmail = this.onChangeEmail.bind(this)
      this.onChangePassword = this.onChangePassword.bind(this)
      this.addNewUser = this.addNewUser.bind(this)
   }
   onChangeName(event) {
      this.setState({
         name: event.target.value
      })
   }
   onChangeEmail(event) {
      this.setState({
         email: event.target.value
      })
   }
   onChangePassword(event) {
      this.setState({
         password: event.target.value
      })
   }
   addNewUser() {
      fetch('https://test-binar.herokuapp.com/auth/signup',
         {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
               'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
               name: this.state.name,
               email: this.state.email,
               password: this.state.password,
            }),
         })
         .then(response => response.json())
         .then(() => {
            this.setState({
               redirect: true
            })
         })
         .catch(err => {
            console.log(err)
         })
   }


   render() {
      if (this.state.redirect) {
         return (
            <Redirect to='/'></Redirect>
         )
      } else if (localStorage.getItem('isLoggedIn')) {
         return (
            <Redirect to='/dashboard'></Redirect>
         )
      }
      return (
         <Form style={{ width: '40vw', margin: 'auto' }}>
            <FormGroup>
               <Input type="text" onChange={this.onChangeName} placeholder="Name" required />
            </FormGroup>
            <FormGroup>
               <Input type="email" onChange={this.onChangeEmail} placeholder="Email" required />
            </FormGroup>
            <FormGroup>
               <Input type="password" onChange={this.onChangePassword} placeholder="Password" required />
            </FormGroup>
            <Link to='/'><Button onClick={this.addNewUser}>Sign Up</Button></Link>
            <FormText>Sudah punya akun? masuk disini..</FormText>
            <Link to='/'><Button>Login</Button></Link>
         </Form>
      );
   }
}