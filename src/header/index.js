import React from 'react';
import {
   Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem,
   NavLink, Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Input
} from 'reactstrap';
import { withRouter, Link } from "react-router-dom";


export default withRouter(class Header extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         isOpenHamburger: false,
         isOpenModal: false,
         modal: false,
         productName: '',
         price: '',
         imageUrl: '',
      }
      this.btntoggle = this.btntoggle.bind(this)
      this.toggleModal = this.toggleModal.bind(this);
      this.toggleHamburger = this.toggleHamburger.bind(this);
      this.onChangeProductName = this.onChangeProductName.bind(this)
      this.onChangePrice = this.onChangePrice.bind(this)
      this.onChangeImageUrl = this.onChangeImageUrl.bind(this)
      this.createNewProduct = this.createNewProduct.bind(this)

   }
   btntoggle() {
      this.setState(prevState => ({ modal: !prevState.modal }));
   }
   toggleHamburger() {
      this.setState({
         isOpenHamburger: !this.state.isOpenHamburger
      });
   }
   toggleModal() {
      this.setState({ isOpenModal: !this.state.isOpenModal });
   }
   onChangeProductName(event) {
      this.setState({
         productName: event.target.value
      })
   }
   onChangePrice(event) {
      this.setState({
         price: event.target.value
      })
   }
   onChangeImageUrl(event) {
      this.setState({
         imageUrl: event.target.value
      })
   }
   createNewProduct() {
      fetch('https://test-binar.herokuapp.com/v1/products/',
         {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
               'Content-Type': 'application/json',
               'Authorization': `${localStorage.getItem('token')}`
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify({
               name: this.state.productName,
               price: this.state.price,
               imageurl: this.state.imageUrl,
            }),
         })
         .then(response => response.json())
         .then(() => {
            this.setState({
               modal: false,
               redirect: true
            })
         })
         .catch(err => {
            console.log(err)
         })
   }
   logout() {
      localStorage.removeItem('isLoggedIn')
      localStorage.removeItem('token')
   }

   render() {
      var tombol
      if (localStorage.getItem('isLoggedIn')) {
         tombol =
            <Nav className="ml-auto" navbar>
               <Button onClick={this.btntoggle}>Create New</Button>
               <NavItem>
                  <NavLink href='/' onClick={this.logout}>LogOut</NavLink>
               </NavItem>
            </Nav>
      } else {
         tombol =
            <Nav className="ml-auto" navbar>
               <NavItem>
                  <NavLink href='/'>Login</NavLink>
               </NavItem>
               <NavItem>
                  <NavLink href='/register'>Register</NavLink>
               </NavItem>
            </Nav>
      }

      return (
         <div>
            <Navbar color="light" light expand="md">
               <NavbarBrand>List Product</NavbarBrand>
               <NavbarToggler onClick={this.toggleHamburger} />
               <Collapse isOpen={this.state.isOpenHamburger} navbar>
                  {tombol}
               </Collapse>
            </Navbar>

            <Modal isOpen={this.state.modal} toggle={this.btntoggle}>
               <ModalHeader>Create New</ModalHeader>
               <ModalBody>
                  <FormGroup>
                     <Input type="text" onChange={this.onChangeProductName} placeholder="Product Name" required />
                  </FormGroup>
                  <FormGroup>
                     <Input type="number" onChange={this.onChangePrice} placeholder="Price" required />
                  </FormGroup>
                  <FormGroup>
                     <Input type="text" onChange={this.onChangeImageUrl} placeholder="Image URL" required />
                  </FormGroup>
               </ModalBody>
               <ModalFooter>
                  <Button onClick={this.btntoggle}>Back</Button>
                  <Link to='/dashboard'><Button onClick={this.createNewProduct}>Create</Button></Link>
               </ModalFooter>
            </Modal>

         </div>
      );
   }
})